#!/bin/bash
#   Copyright (C) 2013  Michael Kargl <michaelkargl@qkprod.com>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>
#
# ---------------------------------------------------------------------

###### --------------- TODO -----------------
###### --------------------------------------
#  o) Extract quotes better (some quotes are cut off)
#  o) Implement CLI Arguments (--url="http://bash.org/?random",...)




###### ------------- IMPORTS ----------------
###### --------------------------------------
# none




###### ------------ ARGUMENTS ---------------
###### --------------------------------------
# none




###### ----------GLOBAL VARIABLES -----------
###### --------------------------------------
# none




###### ----- INTERNAL VARIABLES ---------------
###### ---only edit if you know what you do----
# none




###### ---------- FUNCTIONS -----------------
###### --------------------------------------

	# Takes a string that contains special encoded characters:
	#  &lt; &gt; &nbsp;
	# and converts them back into their representive characters
	#  <     >    \n
	#
	# Arguments:
	#   $@:   Some string containing specials
	# Returns:
	#   "Decoded" string
	# Example:
	#   Input:  shellutils.special2char html text as follows &lt;body&gt;
	#   Output: "html text as follows <body>"
	function special2char {
		local encoded="$@"
		local decoded=""
		
		decoded=${encoded//&gt;/>}
		decoded=${decoded//&lt;/<}
		decoded=${decoded//&nbsp;/\n}
		
		echo $decoded
	}

	

	# Takes a string that contains unencoded special characters:
	# >, <, \n
	# and encodes them to html entities
	# &gt; &lt; &nbsp;
	#
	# Arguments:
	#   $@: Some string containing unencoded specials
	# Returns:
	#   "Encoded" string
	# Example:
	#   Input:  "html text as follows <body>"
	#   Output: shellutils.special2char html text as follows &lt;body&gt;
	function char2special {
	   local decoded="$@"
	   local encoded=""
	   
	   # replace character with its encoded counterpart
	   encoded=${decoded//</&lt;}
	   encoded=${encoded//>/&gt;}
	   encoded=${encoded//\\t/&nbsp;}
	 
	   echo $encoded
	}
	
	

	# Takes a string that contains special encoded characters:
	#  &lt; &gt; &nbsp;
	# and converts them back into their representive characters
	#  <     >    \n
	#
	# Arguments:
	#   $@:   Some string containing specials
	# Returns:
	#   "Decoded" string
	# Example:
	#   Input:  shellutils.special2char html text as follows &lt;body&gt;
	#   Output: "html text as follows <body>"
	function special2char {
		local encoded="$@"
		local decoded=""
		
		# replace encoded string with its unencoded counterpart
		decoded=${encoded//&gt;/>}
		decoded=${decoded//&lt;/<}
		decoded=${decoded//&nbsp;/\\t}
		
		echo $decoded
	}

	
	
	# Fetches one random quote from bash.org
	# extracts the quote string and writes it to stdout
	#
	# Arguments:
	#   $1:  url pointing to a page containing bash.org quotes
	# Example:
	#   Input:  getQuote "http://bash.org/?random"
	#   Output: #76936 +(355)- [X]<Beige> It's one o'clock.
	#
	function getQuote {
		local url=$1
		local quotes_file=quotes
		
		if [ -z $url ]; then
			echo "function getQuote requires 1 argument"
			echo "Usage: getQuote url"
			echo "getQuote http://bash.org/?random"
			exit
		fi
		
		
		# downloading page containing many quotes
		wget $url -O "$quotes_file"
		
		# get all quoteids and pick one of them randomly
		# replace hash with question mark as requested by bash.org
		# http://bash.org/?7890
		id=$(cat quotes|egrep -o "#[0-9]+"|shuf -n1|sed 's/#/?/')
		
		# get quote page
		wget "http://bash.org/$id" -O "$id"
		
		# get the quote element and remove the surounding tags
		# "<p class="qt"> bla dasb </p>"
		local encoded_quote=$(cat $id |egrep '<p class="quote">.+</p>'|sed -e 's/<[^>]*>//g')
		
		# decode special characters like <, >, \t
		special2char $encoded_quote
		
		rm "$quotes_file"
		rm "$id"
	}

	
###### ---------- MAIN PROGRAM --------------
###### --------------------------------------
	getQuote "http://bash.org/?random"
